﻿using System.Threading.Tasks;
using JwtAuth.Models;
using JwtAuth.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JwtAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IAuthenticationService _authenticationService;
        private readonly Services.Interfaces.IAuthorizationService _jwtAuthorizationService;

        public LoginController(IConfiguration configuration, 
            IAuthenticationService authenticationService,
            Services.Interfaces.IAuthorizationService jwtAuthorizationService)
        {
            _configuration = configuration;
            _authenticationService = authenticationService;
            _jwtAuthorizationService = jwtAuthorizationService;
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLogin user)
        {
            var _userInfo = await _authenticationService.ValidateUserAsync(user.UserId, user.Password);
            if (_userInfo != null)
            {
                if(_jwtAuthorizationService.IsAuthenticated(_userInfo, out string token))
                    return Ok(new { token });
            }
            
                return Unauthorized();
            
        }
  

    }
}