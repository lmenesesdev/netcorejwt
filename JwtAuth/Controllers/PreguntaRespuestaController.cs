﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JwtAuth.Models;
using JwtAuth.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JwtAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PreguntaRespuestaController : ControllerBase
    {
        private readonly PreguntaRespuestaService _service;

        public PreguntaRespuestaController(PreguntaRespuestaService service)
        {
            _service = service;
        }

        // GET: api/Colaboradores
        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<IEnumerable<PreguntasRespuestas>>> Get()
        {

            try
            {
                return Ok(await _service.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            
        }

        // GET: api/Colaboradores
        [HttpGet]
        [Route("GetById")]
        public async Task<ActionResult<PreguntasRespuestas>> GetById([FromQuery]int id)
        {
            try
            {
                return Ok(await _service.GetById(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }


        // GET: api/Colaboradores
        [HttpPost]
        [Route("Add")]
        public async Task<ActionResult<bool>> Add([FromBody]PreguntasRespuestas obj)
        {
            try
            {
                return Ok(await _service.Add(obj));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
           
        }


        // GET: api/Colaboradores
        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<bool>> Update([FromBody]PreguntasRespuestas obj)
        {
            try
            {
                return Ok(await _service.Update(obj));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            
        }


        // GET: api/Colaboradores
        [HttpDelete]
        [Route("Delete")]
        public async Task<ActionResult<bool>> Delete([FromQuery]int id)
        {
            try
            {
                return Ok(await _service.Delete(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
           
        }

    }
}