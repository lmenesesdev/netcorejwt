﻿using JwtAuth.Models;
using JwtAuth.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JwtAuth.Auth.Services
{
    public class JwtAuthorizationService : IAuthorizationService
    {

        private readonly JwtTokenData _jwtTokenData;

        public JwtAuthorizationService(IOptions<JwtTokenData> jwtTokenData)
        {
            _jwtTokenData = jwtTokenData.Value;
        }


        public bool IsAuthenticated(User userInfo, out string token)
        {
            try
            {

                var symmetricSecurityKey = new SymmetricSecurityKey(
                       Encoding.UTF8.GetBytes(_jwtTokenData.SecretKey)
                   );
                var signingCredentials = new SigningCredentials(
                        symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                    );
                var header = new JwtHeader(signingCredentials);

                var claims = new[]
                {
                new Claim(ClaimTypes.NameIdentifier, userInfo.UserId),
                new Claim(ClaimTypes.Email, userInfo.Email)
            };


                var payload = new JwtPayload(
                        issuer: _jwtTokenData.Issuer,
                        audience: _jwtTokenData.Audience,
                        claims: claims,
                        notBefore: DateTime.UtcNow,
                        expires: DateTime.UtcNow.AddMinutes(30)
                    );


                var tokenSecurity = new JwtSecurityToken(
                        header,
                        payload
                    );

                token = new JwtSecurityTokenHandler().WriteToken(tokenSecurity);
                return true;
            }
            catch (Exception ex)
            {
                token = null;
                return false;
            }
        }
    }
}
