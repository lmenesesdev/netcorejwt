﻿using JwtAuth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JwtAuth.Services.Interfaces
{
    public interface IAuthorizationService
    {
        bool IsAuthenticated(User userInfo, out string token);
    }
}
