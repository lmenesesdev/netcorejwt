﻿using JwtAuth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JwtAuth.Services.Interfaces
{
    public interface IAuthenticationService
    {
        bool IsValidUser(string usuario, string password);
        User ValidateUser(string usuario, string password);
        Task<bool> IsValidUserAsync(string usuario, string password);
        Task<User> ValidateUserAsync(string usuario, string password);
    }
}
