﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JwtAuth.Models;
using JwtAuth.Repository;
using JwtAuth.Repository.Interfaces;

namespace JwtAuth.Services
{
    public class PreguntaRespuestaService
    {
        private readonly IRepository<PreguntasRespuestas> _repo;

        public PreguntaRespuestaService(IRepository<PreguntasRespuestas> repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<PreguntasRespuestas>> GetAll()
        {
            return _repo.GetAll();
        }

        public async Task<PreguntasRespuestas> GetById(int id)
        {
            return _repo.GetById(id);
        }

        public async Task<bool> Add(PreguntasRespuestas obj)
        {
            return _repo.Add(obj);
        }

        public async Task<bool> Update(PreguntasRespuestas obj)
        {
            return _repo.Update(obj);
        }

        public async Task<bool> Delete(int id)
        {
            return _repo.Delete(id);
        }
    }
}
