﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JwtAuth.Models
{
    public class UserLogin
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
