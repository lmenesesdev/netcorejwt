﻿using NPoco;

namespace JwtAuth.Models
{
    [TableName("Preguntasyrespuestas")]
    [PrimaryKey("Idpyr")]
    public class PreguntasRespuestas
    {
        public int Idpyr { get; set; }
        public string Pregunta { get; set; }
        public string Respuesta { get; set; }
    }
}
