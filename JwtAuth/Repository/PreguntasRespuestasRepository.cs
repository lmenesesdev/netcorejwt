﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JwtAuth.Db.Interface;
using JwtAuth.Models;
using JwtAuth.Repository.Interfaces;
using NPoco;

namespace JwtAuth.Repository
{
    public class PreguntasRespuestasRepository : IRepository<PreguntasRespuestas>
    {
        private readonly IDatabase _db;

        public PreguntasRespuestasRepository(IDbContext db)
        {
            _db = db.GetDb();
        }

        public bool Add(PreguntasRespuestas obj)
        {
            return _db.Insert<PreguntasRespuestas>(obj) != null;
        }

        public bool Delete(int id)
        {
            return _db.Delete<PreguntasRespuestas>(id) > 0;
        }

        public IList<PreguntasRespuestas> GetAll()
        {
            string query = "SELECT * FROM Preguntasyrespuestas";
            IList<PreguntasRespuestas> resultList = _db.Fetch<PreguntasRespuestas>(query);

            return resultList;
        }

        public PreguntasRespuestas GetById(int id)
        {
            var result = _db.SingleById<PreguntasRespuestas>(id);
            return result;
        }

        public bool Update(PreguntasRespuestas obj)
        {      
            return _db.Update(obj) > 0;
        }
    }
}
