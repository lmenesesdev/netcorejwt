﻿using System.Collections.Generic;


namespace JwtAuth.Repository.Interfaces
{
    public interface IRepository<TEntity>
    {
        IList<TEntity> GetAll();
        TEntity GetById(int idCustomer);
        bool Add(TEntity customer);
        bool Update(TEntity customer);
        bool Delete(int idCustomer);
    }
}
