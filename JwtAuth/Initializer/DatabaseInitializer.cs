﻿using JwtAuth.Db;
using JwtAuth.Db.Interface;
using Microsoft.Extensions.DependencyInjection;
using NPoco;


namespace JwtAuth.Initializer
{
    public class DatabaseInitializer
    {
        public static void LoadDb(IServiceCollection services)
        {          
            services.AddScoped<IDbContext, DbContext>();
        }

    }
}
