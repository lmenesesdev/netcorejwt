﻿using JwtAuth.Auth.Services;
using JwtAuth.Models;
using JwtAuth.Repository;
using JwtAuth.Repository.Interfaces;
using JwtAuth.Services;
using JwtAuth.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;


namespace JwtAuth.Initializer
{
    public static class ServicesInitializer
    {

        public static void LoadServices(IServiceCollection services) {

            //Services          
            services.AddScoped<Services.Interfaces.IAuthenticationService, AuthenticacionService>();
            services.AddScoped<IAuthorizationService, JwtAuthorizationService>();
            services.AddScoped<PreguntaRespuestaService>();

            //Repositorys
            services.AddScoped<IRepository<PreguntasRespuestas>, PreguntasRespuestasRepository>();
        }

    }
}
