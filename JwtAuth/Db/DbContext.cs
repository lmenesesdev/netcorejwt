﻿using System.Data.SqlClient;
using JwtAuth.Db.Interface;
using JwtAuth.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using NPoco;


namespace JwtAuth.Db
{
    public class DbContext : IDbContext
    {
        private readonly IDatabase _db;

        public DbContext(IConfiguration config)
        {
            _db = new Database(config.GetConnectionString("DefaultConnection"), DatabaseType.SqlServer2012, SqlClientFactory.Instance);
        }

        public IDatabase GetDb()
        {
            return _db;
        }

    }
}
