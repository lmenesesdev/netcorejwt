﻿using NPoco;

namespace JwtAuth.Db.Interface
{
    public interface IDbContext
    {
         IDatabase GetDb();
    }
}
